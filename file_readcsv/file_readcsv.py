# Read content from csv file and display result in console

import csv

class Person:
  nom = ''
  prenom = ''
  age = 0
  def __init__(self, nom, prenom, age):
    self.nom = nom
    self.prenom = prenom
    self.age = age

  def display(self):
    print self.nom, ' ', self.prenom, ' ', self.age

persons = []
with open('persons.csv', 'r') as f:
  personreader = csv.reader(f, delimiter=',', quotechar='|')
  for elts in personreader:
    person = Person(elts[0].strip(), elts[1].strip(), (int)(elts[2]))
    persons.append(person)

  for person in persons:
    person.display()