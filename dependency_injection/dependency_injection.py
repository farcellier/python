# Utiliser un logger avec l'injection de dependance

import logging
from container import Container
 
class MathService:
  def __init__(self, logger):
    self.logger = logger

  def average(self, numbers):
    self.logger.warning('average - numbers - {0}'.format(numbers))
    total = 0
    for number in numbers:
      total += number

    result =  total / len(numbers)
    self.logger.warning('average - result - {0}'.format(result))
    return result

class ConsoleLoggingService:
  def __init__(self):
    pass

  def warning(self, value):
    print ('WARNING:{0}'.format(value))

c = Container()
c['logging'] = ConsoleLoggingService()
#c['logging'] = logging
c['MathService'] = MathService(c['logging'])

ms = c['MathService']
print ms.average([5, 10, 18])