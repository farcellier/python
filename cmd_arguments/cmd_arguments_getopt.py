#!/usr/bin/python

import sys, getopt

def main(argv):
    
    if len(argv) == 0:
        usage()
        sys.exit()

    log = "/var/log"
    try:
        opts, args = getopt.getopt(argv, "hl:d", ["help", "log=", "debug"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    print opts
    print args
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt == '-d':
            debug = 1
        elif opt in ("-l", "--log"):
            log = arg

def usage():
    print "python %s (-h|-d) [-l log_path] files" % (sys.argv[0])

#...

if __name__ == "__main__":
    main(sys.argv[1:])
