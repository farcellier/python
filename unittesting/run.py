#!/usr/bin/python

import unittest
import sys
import os
import argparse

def main():
  parser = argparse.ArgumentParser(description='Runner with in the project')
  parser.add_argument('command', choices=['tests'])
  args = parser.parse_args()

  appendSysPath("src")
  appendSysPath("tests")

  if args.command == "tests":
    all_tests = unittest.TestLoader().discover("tests")
    unittest.TextTestRunner().run(all_tests)

def appendSysPath(path):
  basename=os.path.dirname(__file__)
  sys.path.insert(1, os.path.abspath(os.path.join(basename,path)))

if __name__ == "__main__":
  main()