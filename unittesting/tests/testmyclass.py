import myclass
import unittest

class TestMyclass(unittest.TestCase):
    def setUp(self):
        self._tested = myclass.Myclass()
    
    def test_add_Given_2and2_Then_Returns4(self):
        # Acts
        result = self._tested.add(2, 2)
        # Assert
        self.assertEqual(4, result)
        
    def test_add_Given_4and2_Then_Returns6(self):
        # Acts
        result = self._tested.add(4, 2)
        # Assert
        self.assertEqual(6, result)